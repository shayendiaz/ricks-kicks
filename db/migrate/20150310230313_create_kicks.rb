class CreateKicks < ActiveRecord::Migration
  def change
    create_table :kicks do |t|
      t.string :brand
      t.string :model
      t.string :colorway
      t.string :lowmidhigh
      t.integer :price
      t.boolean :forsale

      t.timestamps null: false
    end
  end
end
