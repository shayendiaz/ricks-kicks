class User < ActiveRecord::Base
  has_secure_password

  has_many :kicks

  validates :password, presence: true, on: :create, confirmation: true
end
