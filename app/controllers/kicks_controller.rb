class KicksController < ApplicationController
  def index
    @kicks = Kick.all
  end

  def show
    @kick = Kick.find_by_id(params[:email][:id])
  end

  def new
    @kick = Kick.new 
  end

  def create
    @kick = Kick.new(kick_params)

    if @kicks.save
      flash[:notice] = "Kicks were successfully created!"
      redirect_to kicks_path
    else
      flash.now[:alert] = "Could not create your Kicks."
      render :new
    end
  end

  def edit
  end

  def update
  end

  def destroy
  end
end